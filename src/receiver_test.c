/*
===============================================================================
 Name        : testUART2.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include "system_LPC17xx.h"
#include "lpc17xx_clkpwr.h"

#include <cr_section_macros.h>

// TODO: insert other include files here

// TODO: insert other definitions and declarations here
#define UART_THR_MASKBIT   	((uint8_t)0xFF) 		/*!< UART Transmit Holding mask bit (8 bits) */

/*********************************************************************//**
 * Macro defines for Macro defines for UARTn Divisor Latch LSB register
 **********************************************************************/
#define UART_LOAD_DLL(div)	((div) & 0xFF)	/**< Macro for loading least significant halfs of divisors */
#define UART_DLL_MASKBIT	((uint8_t)0xFF)	/*!< Divisor latch LSB bit mask */

/*********************************************************************//**
 * Macro defines for Macro defines for UARTn Divisor Latch MSB register
 **********************************************************************/
#define UART_DLM_MASKBIT	((uint8_t)0xFF)			/*!< Divisor latch MSB bit mask */
#define UART_LOAD_DLM(div)  (((div) >> 8) & 0xFF)	/**< Macro for loading most significant halfs of divisors */
#define UART_LCR_DLAB_EN		((uint8_t)(1<<7))    	/*!< UART Divisor Latches Access bit enable */
#define UART_LCR_BITMASK		((uint8_t)(0xFF))		/*!< UART line control bit mask */
#define UART_FDR_DIVADDVAL(n)	((uint32_t)(n&0x0F))		/**< Baud-rate generation pre-scaler divisor */
#define UART_FDR_MULVAL(n)		((uint32_t)((n<<4)&0xF0))	/**< Baud-rate pre-scaler multiplier value */
#define UART_FDR_BITMASK		((uint32_t)(0xFF))			/**< UART Fractional Divider register bit mask */
#define PCOMP (*(unsigned int *)  0x400FC0C4)
#define PCLKSEL0 (*(unsigned int *)  0x400FC1A8)
#define U0LCR (*(unsigned int *)  0x4000C00C)
#define U0DLL (*(unsigned int *)   0x4000C000)
#define U0DLM (*(unsigned int *)   0x4000C004)
#define U0FCR (*(unsigned int *)  0x4000C008)
#define PINSEL0 (*(unsigned int *)  0x4002C000)
#define U0IER (*(unsigned int *)  0x4000C004)
#define ISER0 (*(unsigned int *)  0xE000E100)
#define U0THR (*(unsigned int *)  0x4000C000)

#define PCLKSEL1 (*(unsigned int *)   0x400FC1AC)
#define U2LCR (*(unsigned int *)   0x4009800C)
#define U2DLL (*(unsigned int *)   0x40098000)
#define U2DLM (*(unsigned int *)   0x40098004)
#define U2FCR (*(unsigned int *)  0x40098008)
#define U2FDR (*(unsigned int *)  0x40098028)
#define U2IER (*(unsigned int *)  0x40098004)
#define U2RBR (*(unsigned int *)   0x40098000)
#define U2LSR (*(unsigned int *)   0x40098014)
#define U2THR (*(unsigned int *)   0x40098000)

#define TRUE 1
#define FALSE 0

#define SEQUENCE 0xABCD

int init = FALSE;
uint16_t init_sequence = 0;

uint8_t count = 0;
uint8_t buf[4];
long* q = 0;
uint8_t valid_data;

int main(void) {


	SystemInit();

	PCOMP |= 0x01 << 24;			//turn on UART2
	PCLKSEL1 |= 0x1 << 16;
	PCLKSEL1 &= ~(0x1 << 17);			//set UART1 clk
	U2LCR |= 0x01 << 7;				//enable Divisor LatchAccess Bit (DLAB)
	U2FDR &= ~0xFF;
	U2FDR |= 0x21;
	U2DLL &= ~0xFF;
	U2DLL |= 0x04;
	U2DLM &= ~0xFF;
	U2DLM |= 0x03;
	U2FCR |= 0x01;					//enable UARTFIFO
	PINSEL0 |= 0x01 << 20;
	PCLKSEL0 &= ~0x1 << 21;
	PINSEL0 |= 0x01 << 22;
	PCLKSEL0 &= ~0x1 << 23;			//pin P0.2 TX0
	U2LCR &= ~0x01 << 7;			//disable Divisor LatchAccess Bit (DLAB)
	U2IER |= 0x01;					//enable RBR interrupt
	ISER0 |= 0x01 << 7;				//enable interrupts from UART2

	U2LCR &= ~0xFF;
	U2LCR |= 0x3;



    // TODO: insert code here

    // Force the counter to be placed into memory
    volatile static int i = 0 ;

    uint32_t baudrate = 9600;



	uint32_t uClk;
	uint32_t d, m, bestd, bestm, tmp;
	uint64_t best_divisor, divisor;
	uint32_t current_error, best_error;
	uint32_t recalcbaud;

	best_error = 0xFFFFFFFF; /* Worst case */
	bestd = 0;
	bestm = 0;
	best_divisor = 0;

    uClk = CLKPWR_GetPCLK (CLKPWR_PCLKSEL_UART2);
    for ( m = 1 ; m <= 15 ;m++)
    {
    	for ( d = 0 ; d < m ; d++)
    	{
    	  divisor = ((uint64_t)uClk<<28)*m;
    	  divisor /= ((uint64_t)baudrate*(m+d));
    	  current_error = divisor & 0xFFFFFFFF;
   		  tmp = divisor>>32;
   		  /* Adjust error */
   		  if(current_error > ((uint32_t)1<<31)){
   			current_error = -current_error;
   			tmp++;
   			}

   		  if(tmp<1 || tmp>65536) /* Out of range */
   		  continue;

   		  if( current_error < best_error){
   			best_error = current_error;
   			best_divisor = tmp;
   			bestd = d;
   			bestm = m;
   			if(best_error == 0) break;
   			}
   		} /* end of inner for loop */

   		if (best_error == 0)
   		  break;
    	} /* end of outer for loop  */

    	if(best_divisor == 0) return ERROR; /* can not find best match */

    	recalcbaud = (uClk>>4) * bestm/(best_divisor * (bestm + bestd));

    	/* reuse best_error to evaluate baud error*/
    	if(baudrate>recalcbaud) best_error = baudrate - recalcbaud;
    	else best_error = recalcbaud -baudrate;

    	best_error = best_error * 100 / baudrate;


    	U2LCR |= UART_LCR_DLAB_EN;
    	U2FDR &= ~0xFF;
    	U2DLL &= ~0xFF;
    	U2DLM &= ~0xFF;
    	U2DLM = UART_LOAD_DLM(best_divisor);
   		U2DLL = UART_LOAD_DLL(best_divisor);
  		/* Then reset DLAB bit */
   		U2LCR &= (~UART_LCR_DLAB_EN) & UART_LCR_BITMASK;
   		U2FDR = (UART_FDR_MULVAL(bestm) |
   		UART_FDR_DIVADDVAL(bestd)) & UART_FDR_BITMASK;


    // Enter an infinite loop, just incrementing a counter
    while(1) {
        //i++ ;
       // printf("yaaas");
    	//int data = U2RBR;
    	//printf("%d\n", 0);

    }
    return 0 ;
}

void UART2_IRQHandler(void){
	if(init == FALSE)
	{
		uint8_t data = U2RBR;
		init_sequence = (init_sequence << 4) | data;

		if(init_sequence == SEQUENCE)
		{
			init = TRUE;
		}
	}else
	{

		int status = U2LSR;
		int data = U2RBR;
		printf("%d\n", data);
		while(!((U2LSR & 0x20) > 1));
		U2THR |= data;
	}
}




